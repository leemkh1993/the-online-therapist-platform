import express from 'express';
import { io, database } from './server';

export const patientRoutes = express.Router();

// patientRoutes.use('/patient/record', checkLoggedInAPI, checkPatientAPI, express.static('record'));

// Route for session request and creating match
patientRoutes.post('/initiation', async function (req, res) {
    try {
        if (req.session) {
            const userEmail = req.session.email; // Get patient email
            const getPatientData = await database.query(/*sql*/ `select users.first_name, patient_id, patient_roomkey from patients join users on patients.user_id = users.user_id where users.email = $1`, [userEmail]);
            const patientName = getPatientData.rows[0].first_name;
            const patientId = getPatientData.rows[0].patient_id;
            const patientRoomKey = getPatientData.rows[0].patient_roomkey;
            const therapistName = req.body.tName;
            const getTherapistRoomKey = await database.query(/*sql*/ `select therapist_roomkey, therapist_id from therapists join users on therapists.user_id = users.user_id where users.first_name = $1`, [therapistName]);
            const therapistId = getTherapistRoomKey.rows[0].therapist_id;
            const therapistRoomKey: string = getTherapistRoomKey.rows[0].therapist_roomkey;
            const getMatchRoomKey = await database.query(/*sql*/ `select room_key from matches where matches.therapist_id = $1 and matches.patient_id = $2`, [therapistId, patientId]);
            if (getMatchRoomKey.rows) {
                const matchRoomKey = getMatchRoomKey.rows[0].room_key;
                io.to(therapistRoomKey).emit('sessionRequest', { patientName: patientName, patientRoomKey: patientRoomKey, therapistRoomKey: therapistRoomKey, matchRoomKey: matchRoomKey });
                res.status(200).json({ MatchFound: true, sessionRequest: true });
            } else {
                const roomKey = `${patientName}_${patientId}_${therapistName}_${therapistId}`
                const createMatch = await database.query(/*sql*/ `insert into matches (patient_id, therapist_id, room_key) values ($1, $2, $3`, [patientId, roomKey]);
                console.log(createMatch.rows);
                io.to(therapistRoomKey).emit('sessionRequest', { patientName: patientName, patientRoomKey: patientRoomKey, therapistRoomKey: therapistRoomKey, matchRoomKey: roomKey});
                res.status(200).json({ MatchCreated: true, sessionRequest: true });
            };
        };
    } catch (e) {
        console.log(e);
        res.status(500).json({ MatchFailed: true });
    };
});

type therapistData = {
    firstName: string,
    lastName: string,
    session: string[]
}

patientRoutes.get('/patient/record/session_record', async function (req, res) {
    try {
        if (req.session) {
            // Get current patient
            const email = req.session.email;
            // Get the patient id
            const getId = await database.query(/*sql*/ `select * from patients join users on patients.user_id = users.user_id where users.email = $1`, [email]);
            const patientId = getId.rows[0].patient_id;
            const getMatches = await database.query(/*sql*/ `select * from matches where matches.patient_id = $1`, [patientId]);
            const matches = getMatches.rows;

            if (matches != undefined) {
                const therapistsData: therapistData[] = []

                for (let match of matches) {
                    const matchId = match.match_id;
                    const therapistId = match.therapist_id;
                    const getNames = await database.query(/*sql*/ `select * from therapists join users on therapists.user_id = users.user_id where therapists.user_id = $1`, [therapistId]);
                    const firstName = getNames.rows[0].first_name;
                    const lastName = getNames.rows[0].last_name;
                    const getSessions = await database.query(/*sql*/ `select * from thera_sessions where thera_sessions.match_id = $1 order by date`, [matchId])
                    const sessions: string[] = []

                    for (let session of getSessions.rows) {
                        const str = JSON.stringify(session);
                        sessions.push(str)
                    }

                    const therapistData = {
                        firstName: firstName,
                        lastName: lastName,
                        sessions: sessions
                    }
                    console.log(therapistData)
                    // therapistsData.push(therapistData)
                }
                res.status(200).json(therapistsData);
            } else {
                res.status(204).json({ emptyList: true });
            }
        }
    } catch (e) {
        console.log(e);
    }
});
