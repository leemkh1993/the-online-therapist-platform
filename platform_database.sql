create database platform;

\c platform;

create table users(
    user_id SERIAL primary key,
    email VARCHAR(255) not null,
    password VARCHAR(255) not null,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    age_group VARCHAR(255),
    gender VARCHAR(255),
    address VARCHAR(255),
    created TIMESTAMP,
    updated TIMESTAMP
);

create table therapists (
    therapist_id serial primary key,
    user_id integer not null,
    foreign key (user_id) references users(user_id),
    therapist_roomkey VARCHAR(255)
);

create table patients (
    patient_id serial primary key,
    user_id integer not null,
    foreign key (user_id) references users(user_id),
    patient_roomkey VARCHAR(255)
);

create table survey_records (
    survey_record_id serial primary key,
    patient_id integer,
    foreign key (patient_id) references patients(patient_id),
    response JSON not null,
    created TIMESTAMP
);

create table matches (
    match_id serial primary key,
    patient_id integer not null,
    foreign key (patient_id) references patients(patient_id),
    therapist_id integer not null,
    foreign key (therapist_id) references therapists(therapist_id),
    room_key varchar(255) not null
);

create table thera_sessions (
    thera_session_id serial primary key,
    date TIMESTAMP,
    match_id integer,
    foreign key (match_id) references matches(match_id),
    note text
);

alter TABLE patients add column survey_record_id INTEGER;
alter table patients add foreign key (survey_record_id) references survey_records(survey_record_id);

insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('the1@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', '艾莉絲', 'lee', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('the2@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', '蕾夢娜', 'kong', '18', 'F', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('the3@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', '馬特史密斯', 'choi', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('the4@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', '花生醬先生', 'lee', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('the5@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', '泰坦斯', 'kong', '18', 'F', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('the6@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', '派佩', 'choi', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('the7@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', '娃娃', 'choi', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('the8@gmail.com', '$2a$10$/OEc5vi64Xy8E69vLVI6AOGPfs9zbec5cJMa8KvXUMWSIVA0e9fOy', 'Zen13', 'GSU', '1000', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('pat1@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', 'patient', '1', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('pat2@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', 'patient', '2', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('pat3@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', 'patient', '3', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('pat4@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', 'patient', '4', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('pat5@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', 'patient', '5', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('pat6@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', 'patient', '6', '18', 'M', 'Home', now(), now());
insert into users (email, password, first_name, last_name, age_group, gender, address, created, updated) values ('pat7@gmail.com', '$2a$10$GvW8QD95WrU.J/9cpVuKt.GOnangmohTsMMCdgfbZP5aaO.oPPJXW', 'patient', '7', '18', 'M', 'Home', now(), now());

insert into therapists (user_id, therapist_roomkey) values ((select user_id from users where email = 'the1@gmail.com'), now());
insert into therapists (user_id, therapist_roomkey) values ((select user_id from users where email = 'the2@gmail.com'), now());
insert into therapists (user_id, therapist_roomkey) values ((select user_id from users where email = 'the3@gmail.com'), now());
insert into therapists (user_id, therapist_roomkey) values ((select user_id from users where email = 'the4@gmail.com'), now());
insert into therapists (user_id, therapist_roomkey) values ((select user_id from users where email = 'the5@gmail.com'), now());
insert into therapists (user_id, therapist_roomkey) values ((select user_id from users where email = 'the6@gmail.com'), now());
insert into therapists (user_id, therapist_roomkey) values ((select user_id from users where email = 'the7@gmail.com'), now());
insert into patients (user_id, patient_roomkey) values ((select user_id from users where email = 'pat1@gmail.com'), now());
insert into patients (user_id, patient_roomkey) values ((select user_id from users where email = 'pat2@gmail.com'), now());
insert into patients (user_id, patient_roomkey) values ((select user_id from users where email = 'pat3@gmail.com'), now());
insert into patients (user_id, patient_roomkey) values ((select user_id from users where email = 'pat4@gmail.com'), now());
insert into patients (user_id, patient_roomkey) values ((select user_id from users where email = 'pat5@gmail.com'), now());
insert into patients (user_id, patient_roomkey) values ((select user_id from users where email = 'pat6@gmail.com'), now());
insert into patients (user_id, patient_roomkey) values ((select user_id from users where email = 'pat7@gmail.com'), now());

insert into survey_records (patient_id, response, created) values (1, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (1, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (2, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (2, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (3, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (3, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (4, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (4, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (5, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (5, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (6, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (6, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (7, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());
insert into survey_records (patient_id, response, created) values (7, '{ "Symptom": ["虐待暴力", "酒精/藥物濫用", "焦慮", "慢性疼痛", "抑鬱", "慾食失調", "家庭問題"], "intention": "qwfqwfwqfw","Habits": "一周3-4次", "MentalState1": "有", "MentalState2": "沒有", "MentalState3": "有", "MentalState4": "有", "MentalState5": "有", "MentalState6": "有", "MentalState7": "從來沒有" }' ::json, now());


update patients set survey_record_id = 1 where patient_id = 1;
update patients set survey_record_id = 3 where patient_id = 2;
update patients set survey_record_id = 5 where patient_id = 3;
update patients set survey_record_id = 7 where patient_id = 4;
update patients set survey_record_id = 9 where patient_id = 5;
update patients set survey_record_id = 11 where patient_id = 6;
update patients set survey_record_id = 13 where patient_id = 7;

insert into matches (patient_id, therapist_id, room_key) values(1, 1, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(2, 1, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(3, 1, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(4, 1, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(5, 1, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(6, 1, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(7, 1, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(1, 2, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(2, 2, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(3, 2, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(4, 2, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(5, 2, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(6, 2, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(7, 2, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(1, 3, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(2, 3, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(3, 3, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(4, 3, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(5, 3, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(6, 3, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(7, 3, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(1, 4, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(2, 4, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(3, 4, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(4, 4, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(5, 4, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(6, 4, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(7, 4, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(1, 5, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(2, 5, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(3, 5, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(4, 5, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(5, 5, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(6, 5, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(7, 5, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(1, 6, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(2, 6, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(3, 6, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(4, 6, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(5, 6, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(6, 6, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(7, 6, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(1, 7, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(2, 7, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(3, 7, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(4, 7, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(5, 7, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(6, 7, 'room_key');
insert into matches (patient_id, therapist_id, room_key) values(7, 7, 'room_key');

insert into thera_sessions (date, match_id, note) values (now(), 1, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 1, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 2, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 2, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 3, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 3, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 4, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 4, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 5, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 5, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 6, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 6, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 7, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 7, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 8, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 8, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 9, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 9, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 10, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 10, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 11, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 11, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 12, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 12, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 13, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 13, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 14, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 14, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 15, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 15, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 16, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 16, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 17, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 17, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 18, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 18, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 19, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 19, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 20, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 20, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 21, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 21, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 22, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 22, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 23, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 23, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 24, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 24, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 25, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 25, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 26, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 26, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 27, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 27, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 28, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 28, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 29, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 29, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 30, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 30, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 31, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 31, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 32, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 32, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 33, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 33, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 34, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 34, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 35, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 35, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 36, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 36, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 37, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 37, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 38, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 38, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 39, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 39, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 40, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 40, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 41, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 41, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 42, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 42, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 43, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 43, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 44, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 44, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 45, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 45, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 46, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 46, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 47, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 47, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 48, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 48, 'hihihihihi');
insert into thera_sessions (date, match_id, note) values (now(), 49, '1234567890');
insert into thera_sessions (date, match_id, note) values (now(), 49, 'hihihihihi');
