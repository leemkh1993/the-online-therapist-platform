document.querySelector('#signin-form') //class instead of id
    .addEventListener('submit',async function(event){
        event.preventDefault();
            
            const form = event.target;// if consolelog will browe the whole "form" element
            const formObject = {};

            formObject['loginEmail'] = form.loginEmail.value; //?match to html name or database name?
            formObject['loginPassword'] = form.loginPassword.value;

            const res = await fetch('/login',{
                method:"POST",
                headers:{
                    "Content-Type":"application/json"
                },
                body: JSON.stringify(formObject),
            });
            const result = await res.json();

            if(res.status === 200 && result.success && result.role === "Therapist"){ // Marco: Added role so will redirect accordingly
                window.location = "/therapist";
                console.log(formObject['loginEmail']);
                localStorage.setItem("username",formObject['loginEmail']);
            } else if (res.status === 200 && result.success && result.role === "Patient") { // Same as above
                window.location = "/patient";
                localStorage.setItem("username",formObject['loginEmail']);
            } else if (res.status === 401) {
                console.log("login 失敗");
                document.querySelector('#login-message').classList.remove('d-none');
                setTimeout(function(){
                    document.querySelector('#login-message').classList.add('d-none');
                },2500);
            };
    });

//17/7 0919 testdone - switching-tab using link
var btn1 = document.getElementById("Change-to-Register");
btn1.onclick=function a() { 
    $('.nav-tabs li:eq(1) a').tab('show');
    console.log("Change-to-Register");
};

var btn2 = document.getElementById("Change-to-Login");
btn2.onclick=function a() {
    $('.nav-tabs li:eq(0) a').tab('show');
    console.log("Change-to-Login");
};

//. 17/7 0141 form-signup related
document.querySelector('#signup-form')
    .addEventListener('submit',async function(event){
        event.preventDefault();
            
            const form = event.target;// if consolelog will browe the whole "form" element
            const formObject = {};

            formObject['loginEmail'] = form.loginEmail.value;
            formObject['loginPassword'] = form.loginPassword.value;

            const res = await fetch('/register',{
                method:"POST",
                headers:{
                    "Content-Type":"application/json"
                },
            body: JSON.stringify(formObject),
            });
        
        const result = await res.json();

        //not yet link-up
        if(res.status === 200 && result.success){
            document.querySelector('#signup-success-message').classList.remove('d-none');
            setTimeout(function(){
                document.querySelector('#signup-success-message').classList.add('d-none');
                localStorage.setItem("username",formObject['loginEmail']);
                window.location = '/patient'; // Marco: Redirecting back to pat-home-pg
            },3000);
        } else if (res.status === 401) {
            document.querySelector('#signup-fail-message').classList.remove('d-none');
            setTimeout(function(){
                document.querySelector('#signup-fail-message').classList.add('d-none');
            },3000);
        }
    });