import { Response, Request, NextFunction } from "express";

export const checkLoggedInAPI = (req: Request, res: Response, next: NextFunction) => {
    if (req.session && req.session.email) {
        console.log("login check passed")
        next();
    } else {
        res.status(401).redirect('/');
    } 
};

export const checkTherapistAPI = (req: Request, res: Response, next: NextFunction) => {
    if (req.session && req.session.role === "Therapist") {
        console.log("Therapist checked passed")
        next();
    } else {
        res.status(401).redirect('/');
    }
};

export const checkPatientAPI = (req: Request, res: Response, next: NextFunction) => {
    if (req.session && req.session.role === "Patient") {
        console.log("patient check passed")
        next();
    } else {
        res.status(401).redirect('/');
    }
};
