import express, { Request,Response,NextFunction } from "express";
import expressSession from "express-session";
import bodyParser from "body-parser";
// import path from "path";
import pg from "pg";
import http from 'http';
import socketIO from "socket.io";
// import {sessionRoutes} from './sessionRoutes';
import { surveyRoutes } from './surveyRoutes'; 
import { userRoutes } from './userRoutes';
import { registerRoutes } from './registerRoutes';
import { checkLoggedInAPI, checkTherapistAPI, checkPatientAPI } from "./loginCheckings";
import { therapistRoutes } from './therapistRoutes';
import { patientRoutes } from './patientRoutes';
import dotenv from 'dotenv';

dotenv.config();

const app = express();
const server = new http.Server(app);
export const io = socketIO(server);

app.use(bodyParser.urlencoded({ extended: true })); // Form submit
app.use(bodyParser.json()); // json content

// Database Setup and Config
export const database = new pg.Client({
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: "localhost",
    database: process.env.DB_NAME
});

database.connect();

const sessionMiddleware = expressSession({
    secret: "There is no secret",
    resave: true,
    saveUninitialized: true,
    cookie: { secure: 'auto' }
});

app.use(sessionMiddleware);

io.use((socket, next) => {
    sessionMiddleware(socket.request, socket.request.res, next);
});

app.use(function(req:Request,_res:Response,next:NextFunction){
    if(req.session){
        if(!req.session.counter){
            req.session.counter = 1;
        }else{
            req.session.counter += 1;
        }
        console.log(`Counter: ${req.session.counter}`);
    }
    const now = new Date();
    // const today = `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
    console.log(`[${now.toISOString()}] Request ${req.path}`);
    next();
});

//GET method for record of user info
interface USER {
    user_id:number
    email:string
    password:string
    first_name:string
    last_name:string
    gender:string
    contact_no:string
    created:string
    updated:string
    survey_record_id:number,
    patient_id:number,
    response:object,
}

// interface SURVEY_RECORDS {
//     survey_record_id:number,
//     patient_id:number,
//     response:object,
//     created:string
// }

app.get('/user_record',async function(_req,res){
    try{
    const getRecord = await database.query(`SELECT * FROM patients join users on users.user_id = patients.user_id
                                            join survey_records on survey_records.survey_record_id = patients.survey_record_id
                                            `);
    // join survey_records on survey_records.survey_record_id = patients.survey_record_id
    const users:USER[] = getRecord.rows;
    res.json(users);
    // const survey_records:SURVEY_RECORDS[] = getRecord.rows;
    // res.json(survey_records);
    } catch(e){
        // logger.error(e.message);
        res.status(500).json({message:"Failed to get record"});
    }
});



//25.7 logout by LC
app.get('/logout', async function(req,res){
    if (req.session){
        req.session.destroy(function(error) {
            if(error) {
                console.log(error);
            }
            console.log('Logout');
            res.redirect('/');
        });
    } else {
        console.log('Logout btn: Already no req.session')
        res.redirect('/');
    }; 
});

//25.7 display username by LC
app.get('/username_display',async function(req,res){
    try{
    const getRecord2 = await database.query(`SELECT * FROM users
                                            `);
    const users:USER[] = getRecord2.rows;
    res.json(users);
    // const survey_records:SURVEY_RECORDS[] = getRecord.rows;
    // res.json(survey_records);
    } catch(e){
        // logger.error(e.message);
        res.status(500).json({message:"Failed to display name"});
    }
});


// interface Survey{
//     // id:number
//     Q1:string
//     Q2:string
//     Q3:string
//     Q4:string
//     Q5:string
//     Q6:string
//     Q7:string
//     Q8:string
//     Q9:string
//     Q10:string
//     Q11:string
//     Q12:string
//     Q13:string
//     Q14:string
//     Q15:string
//     Q16:string
// }

// const SURVEY_JSON = path.join(__dirname,'survey.json');

// app.post('/survey',async function(req,res){
//     console.log(req.body);
//     const survey:Survey[] = await jsonfile.readFile(SURVEY_JSON);
//     survey.push({
//         Q1: req.body.LastName,
//         Q2: req.body.FirstName,
//         Q3: req.body.Email,
//         Q4: req.body.YourAge,
//         Q5: req.body.Gender,
//         Q6: req.body.YourLocation,
//         Q7: req.body.Symptom,
//         Q8: req.body.Intention,
//         Q9: req.body.Habits,
//         Q10: req.body.MentalState1,
//         Q11: req.body.MentalState2,
//         Q12: req.body.MentalState3,
//         Q13: req.body.MentalState4,
//         Q14: req.body.MentalState5,
//         Q15: req.body.MentalState6,
//         Q16: req.body.MentalState7
//     });
//     await jsonfile.writeFile(SURVEY_JSON,survey,{spaces:2});
//     console.log("survey-push testing");
//     res.status(200).redirect('/patient');//?? seems need adjustment
// });

// app.use('/',express.static('./public', {index: "surveyrecordv2.html"}));

app.use('/',userRoutes); //15/7 21:33 Test success - Changed from /login to / for testing POST Method
app.use('/', express.static('./public/signin', { index: "signin.html" }));

app.use('/', userRoutes);

app.use('/', registerRoutes);

app.use('/therapist', checkLoggedInAPI, checkTherapistAPI, express.static('./public/thera-home-pg', { index: "thera-home-pg.html" }));

app.use('/therapist/members_record', express.static('./public/thera-home-pg/member-record', { index: "surveyrecordv2.html" }));

app.use('/', therapistRoutes);

// app.use('/', sessionRoutes);

app.use('/patient', checkLoggedInAPI, checkPatientAPI, express.static('./public/pat-home-pg', { index: "pat-home-pg.html" }));

app.use('/', patientRoutes);

app.use('/', surveyRoutes);

//24/7 Leo added ugly but workable image route
app.use('/therapist/img', express.static('./public/img'));
app.use('/patient/img', express.static('./public/img'));

// Marco: Now will send all 404 files (naaa not really)
app.use('*', express.static('/public/404-pg', { index: '404.html' }));

const PORT = 8080;

server.listen(PORT, function () {
    console.log(`Listening at http://localhost:${PORT}`);
});

// Below are all socketIO stuff

// When connection is made, will carry out the following:
const onlineTherapists: string[] = [];
const busyTherapists: string[] = [];

io.on('connection', async function (socket) {
    try {
        if (socket.request.session.role === "Therapist") {
            const userEmail: string = socket.request.session.email;
            const getTheraData = await database.query(/*sql*/ `select first_name, therapist_roomkey from therapists join users on therapists.user_id = users.user_id where users.email = $1`, [userEmail]);
            const tName: string = getTheraData.rows[0].first_name;
            const tRoomKey:string = getTheraData.rows[0].therapist_roomkey;
            socket.join(tRoomKey);
            socket.request.session.tName = tName;
            socket.request.session.save();
            onlineTherapists.push(tName)
            io.emit('online', onlineTherapists);
        } else if (socket.request.session.role === "Patient") {
            const userEmail: string = socket.request.session.email;
            const getPatData = await database.query(/*sql*/ `select first_name, patient_roomkey from patients join users on patients.user_id = users.user_id where users.email = $1`, [userEmail]);
            const pName: string = getPatData.rows[0].first_name;
            const pRoomKey: string = getPatData.rows[0].patient_roomkey;
            socket.join(pRoomKey);
            socket.request.session.pName = pName;
            socket.request.session.keys = pRoomKey;
            socket.request.session.save();
            io.emit('online', pName);
        }

        // Therapist side send busy when startSession is confirmed
        socket.on('busy', (_data) => {
            busyTherapists.push(socket.request.session.tName);
            io.emit('busy', busyTherapists);
        });

        // Therapist sent confirmation
        // Data = {patientRoomKey: data.patientRoomKey, matchKey: data.matchRoomKey}
        socket.on('startSession', async function (patientRoomKey, matchRoomKey) {
            io.to(patientRoomKey).emit('session', {matchRoomKey: matchRoomKey});
        });

        socket.on('disconnect', () => {
            if (socket.request.session.tName) {
                for (let i = 0; i < onlineTherapists.length; i++) {
                    if (onlineTherapists[i] == socket.request.session.tName) {
                        onlineTherapists.splice(i, 1) 
                    }
                };
                for (let i = 0; i < busyTherapists.length; i++) {
                    if (busyTherapists[i] == socket.request.session.tName) {
                        busyTherapists.splice(i, 1) 
                    }
                };
                io.emit('offline', socket.request.session.tName);
                socket.request.session.tName = null;
            };
            socket.request.session.keys = null;
            socket.request.session.save();
        });
        
    } catch (e) {
        console.log(e)
        console.log('you here');
    }
});
