
/* Bootstrap scrollspy */
// jquery ready start
//  Activate bootstrap scrollspy
$(document).ready(function () {
	// jQuery code

	////////////////////////  Highlight the top nav as scrolling occurs. /bootstrap/
	$('body').scrollspy({
		target: '#navbar',
		offset: 80
	});
	//////////////////////// Menu scroll to section
	$('.scrollto').click(function (event) {
		let $anchor = $(this);
		$('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 40 }, 1500);
		event.preventDefault();
	});
	///////////////// for small screen
	if ($(window).width() < 992) {

		/////////////////////// Closes the Responsive Menu on Menu Item Click
		$('.navbar-collapse a.scrollto').click(function () {
			$('.navbar-toggler:visible').click();
		});
	}
});


/* Click the Survey Button */

function takeSurveyButton() {
	let trigger = $("body").find('[data-toggle="modal"]');
	trigger.click(function () {
		$('#myModal').modal('hide')
	});
}
takeSurveyButton();


/* Close the popup Survey */
// Popup note when survey successfully submitted
document.querySelector('#submit-button').addEventListener('click', () => {
	document.querySelector('#submit-form-message').classList.remove('d-none');
})

/* close the popup box*/
let closebtns = document.getElementsByClassName("close");
let i;

for (i = 0; i < closebtns.length; i++) {
	closebtns[i].addEventListener("click", function () {
		this.parentElement.style.display = 'none';
	});
}

/* Response to "現在諮詢" Button*/
function TalkToUsNow() {
	const popup = document.getElementById("popUpMsg");
	popup.classList.toggle("show");
	// popup.addEventListener("click",function(){
	// })
}

/* Pop up message selection */
function clickOptionButtons() {
	document.getElementById('serviceFlow').addEventListener("click", function () {
		alert("頁面最底有晒你想知既程序。記住首次諮詢費用全免架，ok先好buy！");
	}, true)

	document.getElementById('mentionIssues').addEventListener("click", function () {
		alert("做左Survey未呢？睇下邊位顧問Online緊，Click個人像就得架喇！");
	}, true);

	document.getElementById('matching').addEventListener("click", function () {
		alert("準備好傾計既話就禁入去下面Chatroom啦!");
	}, true)

	// document.getElementById('talkToUsNow').addEventListener("click", function(){
	// 	alert("確認現在加入視像對話嗎？");
	// }, true)
}
clickOptionButtons();


/* Confirm to start the video */

// const modal2 = document.getElementById("alertMsg");
// const confirmBtn = document.getElementById("confirmMessageBtn");
// const spanX = document.getElementsByClassName("closeTheWindow")[0];

// confirmBtn.onclick = function () {
// 	modal2.style.display = "block";
// }

// spanX.onclick = function () {
// 	modal2.style.display = "none";
// }

// window.onclick = function (event) {
// 	if (event.target == modal2) {
// 		modal2.style.display = "none";
// 	}
}



function runJitsi(matchKey) {
	const jitsiContainer = document.querySelector("#session-container");
	jitsiContainer.innerHTML = " ";
	jitsiContainer.innerHTML += `
		<div class="row" id="session-row">
			<div class="col-12 col-md-8" id="meet">
			</div>
			<div class="col-12 col-md-4">
	  			<div id="text-submit-area">
					<div id="text-area-container">
						<form id="submit-session" action="/submitSession" method="POST">
						  <textarea id="text-area" name="sessionNote"></textarea>
						  <button onclick="submitSession(${matchKey})"type="submit" class="btn session-submit-btn" id="SessionSubmit">Submit Session</button>
						</form>
					</div>
					<div id="button-area-container">
		  				<button onclick="window.location = '/therapist'" type="button" class="btn session-submit-btn">Back To Main Page</button>
					</div>
	  			</div>
			</div>
  		</div>
	`;
	const domain = "meet.jit.si";
	const options = {
		roomName: matchKey,
		width: "100%",
		height: "95%",
		parentNode: document.querySelector('#meet')
	}
	const api = new JitsiMeetExternalAPI(domain, options);
};

async function submitSession(matchKey) {
	document.querySelector("submit-session")
		.addEventListener('submit', async function(event) {
			event.preventDefault();
            
            const form = event.target;
            const formObject = {};

			formObject['note'] = form.sessionNote.value;
			formObject['matchKey'] = matchKey;

            const res = await fetch('/sessionSubmit',{
                method:"POST",
                headers:{
                    "Content-Type":"application/json"
                },
                body: JSON.stringify(formObject),
            });
			const result = await res.json();
			
			if (res.status === 200) {
				document.querySelector("#SessionSubmit").innerHTML = "Success!"
			}
		});
};










/* Preload image*/
// function preloader() {
// 	if (document.getElementById) {
// 		document.getElementById("preload-01").style.background = "url(http://domain.tld/image-01.png) no-repeat -9999px -9999px";
// 		document.getElementById("preload-02").style.background = "url(http://domain.tld/image-02.png) no-repeat -9999px -9999px";
// 		document.getElementById("preload-03").style.background = "url(http://domain.tld/image-03.png) no-repeat -9999px -9999px";
// 	}
// }
// function addLoadEvent(func) {
// 	var oldonload = window.onload;
// 	if (typeof window.onload != 'function') {
// 		window.onload = func;
// 	} else {
// 		window.onload = function() {
// 			if (oldonload) {
// 				oldonload();
// 			}
// 			func();
// 		}
// 	}
// }
// addLoadEvent(preloader);
