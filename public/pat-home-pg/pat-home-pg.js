//25.7 showname-test


let name=localStorage.getItem("username");
console.log(name);

async function showName(){ 
    const res = await fetch('/username_display');
    const user_records = await res.json();

    const nameContainer = document.querySelector('.name-test');
	nameContainer.innerHTML = '';

    for(let user_record of user_records){
        if(name == user_record.email){
        	nameContainer.innerHTML += `${user_record.last_name}${user_record.first_name}`;
		// 	nameContainer.innerHTML += `
		// 	<div>${name}</div>
		// `;
		} else {
			// console.log("else");
		}
    }

    // console.log(ans[12].innerHTML);
    // for (var i = 0; i < ans.length; i++) {
		var name2 = $('.name-test').html();
        if (name2 == "null" || name2 == "nullnull"){
            $('.name-test').html("新用戶您好");
        } else {
			// nameContainer.innerHTML = nameContainer.innerHTML;
			name2 = name2;
        };
            
    // }
};
showName();


//jit.si
// window.onload = () => {
// 	const domain = "meet.jit.si";
// 	const options = {
// 	    roomName: "Match_Room_Key",
// 	    width: 700,
// 	    height: 700,
// 		parentNode: document.querySelector('#jitsi-session')

// 	}
// 	const api = new JitsiMeetExternalAPI(domain, options);
// };

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({
		//  trigger: 'focus',
		 trigger: 'hover',
		 html: true,
		 dataContainer : '.modal-body',
		//  placement: top,
		 content: function () {
			   return '<img class="img-fluid" src="'+$(this).data('img') + '" />';
		 },
		 title: 'Toolbox'
   }) 
});

/* Bootstrap scrollspy */
// jquery ready start
//  Activate bootstrap scrollspy
$(document).ready(function () {
	// jQuery code

	////////////////////////  Highlight the top nav as scrolling occurs. /bootstrap/
	$('body').scrollspy({
		target: '#navbar',
		offset: 80
	});
	//////////////////////// Menu scroll to section
	$('.scrollto').click(function (event) {
		let $anchor = $(this);
		$('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 40 }, 1500);
		event.preventDefault();
	});
	///////////////// for small screen
	if ($(window).width() < 992) {

		/////////////////////// Closes the Responsive Menu on Menu Item Click
		$('.navbar-collapse a.scrollto').click(function () {
			$('.navbar-toggler:visible').click();
		});
	}
});

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({
		 trigger: 'hide',
		//  trigger: 'hover',
		 html: true,
		 dataContainer : '.modal-body',
		 placement: "left",
		 content: function () {
			   return '<img class="img-fluid" src="'+$(this).data('img') + '" />';
		 },
		 title: 'This is how it looks'
   }) 
});

/* Take the Survey Button 
25 Jul: checked by Charlie: this is working */
function takeSurveyButton() {
	// const triggerbtns = $("body").find('[data-toggle="modal"]');
	// // trigger.click(function () {
	// // 	$('#surveyModal').modal('hide')
	// // });
	// // for(let i = 0; i< trigger.length; i++){
	// for(triggerbtn of triggerbtns){
	// triggerbtn[0].addEventListener("click", function(){
	// 	$('#myModal').modal('show');
	// // children > HTML Collection

	// })
	// };
	let trigger = $("body").find('[data-toggle="modal"]');
	trigger.click(function () {
		$('#myModal').modal('hide')
	});

}
takeSurveyButton();


/* Close the popup Box (x) functionable 
25 Jul: checked by Charlie: this is working */
function closeThePopUpBox() {
	const closebtns = document.getElementsByClassName("close");
	const allpopUpBoxElements = document.querySelector("#openbtn");
		
		for(let closebtn of closebtns){
		closebtns[0].addEventListener("click", function(){
		allpopUpBoxElements.style.display="none";
		console.log('clicked open btn');
	});
	}
	}
closeThePopUpBox();


/* Close the popup Survey */
// Popup note when survey successfully submitted
document.querySelector('#submit-button').addEventListener('click', () => {
	document.querySelector('#submit-form-message').classList.remove('d-none');
})


/* Every (.close) Buttons in HTML : 
25 Jul: checked by Charlie: this is working */
function closeSpanX(){
	const closebtns = document.getElementsByClassName("close");
	for (i = 0; i < closebtns.length; i++) {
		closebtns[i].addEventListener("click", function() {
		this.parentElement.style.display = 'none';
		});
	};	
}
closeSpanX();


/* Open Buttons 27_7 by LC
25 Jul: checked by Charlie: this is working */ 
function openThePopUpBox(){
	let openbtns = document.getElementsByClassName("open");
	let x;
	
	for (x = 0; x < openbtns.length; x++) {
		openbtns[x].addEventListener("click", function() {
			document.getElementById("openbtn").style.display= "";
			document.getElementById("openbtn2").style.display="";
		  console.log('clicked open btn');
		});
	  }	
	}
openThePopUpBox();

/* Response to "現在諮詢" Button
25 Jul: checked by Charlie: this is working */
function TalkToUsNow() {
	const popup = document.getElementById("popUpMsg");
	popup.classList.toggle("show");
}


/* Pop up message selection 
25 Jul: checked by Charlie: this is working */
	
function clickOptionButtons() {
	document.getElementById('serviceFees').addEventListener("click", function () {
		alert(" 新戶首次諮詢費用全免架！試左ok先好buy！");
	}, true)
	document.getElementById('matching').addEventListener("click", function () {
		alert("做左Survey未呢？做左導師先知點可以幫到你架！");
	}, true);
	document.getElementById('talkToUsNow').addEventListener("click", function () {
		alert("快啲留意下邊位導師Online緊，再Click去佢地個頭像開始視訊啦！每一位都好搶手架！");
	}, true)
}
clickOptionButtons();
	
// Below is SocketIO stuff

// Initiate a socket
const socket = io.connect();

// Server emit which therapist has just got online
socket.on('online', function (onlineTherapists) {
	// Data is therapist name (string)
	// DOM for changing the color to green and change word from available
	const allElements = Array.from(document.querySelectorAll(".online-therapist"));
	for (let therapist of onlineTherapists) {
		for (let element of allElements) {
			const id = element.getAttribute('id');
			if (id === therapist) {
				element.querySelector("div > div > svg").classList.remove("text-secondary");
				element.querySelector("div > div > svg").classList.add("text-success");
				element.querySelector("div > div > span").innerHTML = "&nbsp; ONLINE";
			} else {
				continue;
			};
		};
	}

});

// Server emit which therapist has just got offline
socket.on('offline', function (data) {
	// Data is therapist name (string)
	// DOM for changing the color to black and change word to offline
	const allElements = Array.from(document.querySelectorAll(".online-therapist"));
	for (let element of allElements) {
		const id = element.getAttribute('id');
		if (id === data) {
			element.querySelector("div > div > svg").classList.remove("text-success");
			element.querySelector("div > div > svg").classList.add("text-secondary");
			element.querySelector("div > div > span").innerHTML = "&nbsp; OFFLINE";
		} else {
			continue;
		};
	};
});

// Server notification for whom will be busy
socket.on('busy', function (busyTherapists) {
	// Data is therapist name (string)
	// DOM for changing the color to red and change word to busy
	const allElements = Array.from(document.querySelectorAll(".online-therapist"));
	for (let therapist of busyTherapists) {
		for (let element of allElements) {
			const id = element.getAttribute('id');
			if (id === therapist) {
				element.querySelector("div > div > svg").classList.remove("text-success");
				element.querySelector("div > div > svg").classList.add("text-warning");
				element.querySelector("div > div > span").innerHTML = "&nbsp; BUSY";
			} else {
				continue;
			};
		}
	};
});

// Receive data when therapist confirmed to start the session
// Data = {matchRoomKey: data.matchRoomKey}
socket.on('session', function (data) {
	console.log(data)
	const jitsiContainer = document.querySelector("#session-container");
	jitsiContainer.innerHTML = " ";
	jitsiContainer.innerHTML += `
		<div class="row" id="session-row">
			<div class="col-12 " id="meet">
			</div>
			<div class="col-12" id="return-button">
		  		<button onclick="window.location = '/patient'" type="button" class="btn session-submit-btn">Back To Main Page</button>
			</div>
  		</div>
	`;
	const domain = "meet.jit.si";
	const options = {
		roomName: data.matchRoomKey,
		width: "100%",
		height: "95%",
		parentNode: document.querySelector('#meet')
	}
	const api = new JitsiMeetExternalAPI(domain, options);
});

// DOM for picking therapists
function pickTherapist(name) {
	const container = document.querySelector('.modal-dialog-2');
	container.innerHTML = " ";
	container.innerHTML += `
		<div class="modal-content-2">
			<div class="modal-header-2">
				<h4 class="modal-title-2" id="myModalLabel">
					你點擊了${name}的視像房間！
	  			</h4>
	  			<div class="modal-body-2">
					你確認現在跟${name}進行視像通話嗎？
					請按確認進入視頻，否則按取消離開。
	  			</div>
	  			<div class="modal-2-buttons">
					<button onclick="confirmTherapist(${name})" type="button" class="btn popup">
		  				確認
					</button>
				<button onclick="closeModal2()" type="button" class="btn btn-default popup" data-dismiss="modal">
		  				取消
				</button>
	  			</div>
			</div>
  		</div>
  		`;
	container.classList.remove('d-none');
};

// DOM for initiating therapist session
async function confirmTherapist(name) {
	const getName = name.getAttribute('id');
	const tName = { tName: getName };
	const req = await fetch('/initiation', {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify(tName)
	});

	const result = await req.json()

	if (req.status === 200 && result.MatchFound) {
		const container = document.querySelector('.modal-dialog-2');
		container.innerHTML = " ";
		container.innerHTML += `
			<div class="modal-content-2">
				<div class="modal-header-2">
	  				<div class="modal-body-2">
						我們收到你的邀請。請等等，正等待導師回覆。。。
	  				</div>
				</div>
  			</div>
			`;
		setInterval(() => {
			container.classList.add('d-none')
		}, 5000)
	};
};

function closeModal2() {
	const container = document.querySelector('.modal-dialog-2');
	container.classList.add('d-none');
}


