import express, { Request, Response, NextFunction } from 'express';
import {database} from './server';


export const surveyRoutes = express.Router();

surveyRoutes.post('/survey', async function (req: Request, res: Response, next: NextFunction) {
    try {
        if (req.session) {
            const email = req.session.email;
            const getId = await database.query(/*sql*/ `select * from patients join users on patients.user_id = users.user_id where users.email = $1`, [email]);
            const patId = getId.rows[0].patient_id;
            const response: any = req.body; // Get JSON string
            const firstName: string= response.FirstName;
            const lastName: string= response.LastName;
            const ageGroup: string= response.YourAge;
            const gender: string= response.Gender;
            const address: string= response.YourLocation;

            const updateUser = await database.query(/*sql*/ `update users set
                                                         first_name = $1,
                                                         last_name = $2, 
                                                         age_group = $3, 
                                                         gender = $4, 
                                                         address = $5,
                                                         updated = NOW() where users.email = $6`, [firstName, lastName, ageGroup, gender,address,email]);
            console.log(updateUser.rows)

            const condition: object = {
                "Symptom": response.Symptom,
                "intention": response.intention,
                "Habits": response.Habits,
                "MentalState1": response.MentalState1,
                "MentalState2": response.MentalState2,
                "MentalState3": response.MentalState3,
                "MentalState4": response.MentalState4,
                "MentalState5": response.MentalState5,
                "MentalState6": response.MentalState6,
                "MentalState7": response.MentalState7
            }
            console.log(condition);
            // const inputSurvey = await database.query(/*sql*/ `insert into survey_records (
            //                                             patient_id,
            //                                             response, 
            //                                             created) values ($1, $2, now())`, 
            //                                             [patId, condition]);
            //vvvvvvvvvvvvv 22/7 since inserted survey_records related once registered, using update instead
            const inputSurvey = await database.query(/*sql*/ `update survey_records 
                                                                set response = ($1), 
                                                                    created = now()
                                                                where patient_id = ($2)`, 
                                                                    [condition, patId]);
            const getsurId = await database.query(/*sql*/ `select survey_record_id from survey_records where patient_id = $1 LIMIT 1`, [patId]);
            const surId = getsurId.rows[0].survey_record_id;
            console.log(`user ID${patId} is doing survey with survey ID${surId}`);
            const inputSurvey2 = await database.query(/*sql*/ `
                                                    update patients 
                                                    set survey_record_id = ($1)
                                                    where patient_id = ($2)
                                                    `,[surId,patId]);
                        
            console.log(inputSurvey.rows);
            console.log(inputSurvey2.rows);
            setTimeout(function () {
                res.status(200).redirect('/patient');
            }, 3000)
        }
    } 
    catch (e) {
        console.log(e)
        res.status(500).json(e);
    }
});