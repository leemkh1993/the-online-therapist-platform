// async function showRecord(){ //Show record for Q7 to Q16
//     const res = await fetch('/survey');
//     const survey_records = await res.json();

// const { getMaxListeners } = require("process");

// const { lookupService } = require("dns");

    
//     const surveyContainer = document.querySelector('.survey-container');
//     surveyContainer.innerHTML = '';

//     for(let survey_record of survey_records){
//         // if(survey_record.patient_id)
//         surveyContainer.innerHTML += `
//                 <div class="survey-intro">
//                 問卷記錄<br>
//             7. 您想在治療中討論和解決哪些問題?(可多項選擇)這個問題必須回答 *<br>
//                 ${survey_record.response.Symptom}<br>
//             8. 您的治療目標是什麼？（請盡量分享你的期望）。 <br>
//                 ${survey_record.response.intention}<br>
//             精神健康狀況，以便提供最適合的方案。<br>
//             9. 您會幾經常使用酒精或毒品？這個問題必須回答*<br>
//                 ${survey_record.response.Habits}<br>
//             10. 您目前正在服用任何處方藥/精神藥物嗎？<br>
//                 ${survey_record.response.MentalState1}<br>
//             11. 您曾否得過任何心理輔導或精神上的支援服務嗎？（例如和治療師，精神病醫生）?<br>
//                 ${survey_record.response.MentalState2}<br>
//             12. 您目前有任何自殘行為嗎？<br>
//                 ${survey_record.response.MentalState3}<br>
//             13. 您現在有想要嚴重傷害任何人嗎？<br>
//                 ${survey_record.response.MentalState4}<br>
//             14. 您曾經嘗試過自殺嗎？<br>
//                 ${survey_record.response.MentalState5}<br>
//             15. 您現在有自殺的念頭嗎？<br>
//                 ${survey_record.response.MentalState6}<br>
//             16. 您最後一次有計畫自殺是什麼時候？<br>
//                 ${survey_record.response.MentalState7}<br>
//           </div>
//         `;
//     }
// }




async function showPatientInfo(){ //show record for Q1-Q6
    const res = await fetch('/user_record');
    const user_records = await res.json();

    const userContainer = document.querySelector('.member-container');
    userContainer.innerHTML = '';
    
    for(let user_record of user_records){
        // if(survey_record.patient_id)
        
        userContainer.innerHTML += `
            <ul class="list-group" id="myList">
                <li>
                    <div class='template'>
                        <div class='template-row first1'>${user_record.last_name}${user_record.first_name}</div>
                        <div class='template-row email'>${user_record.email}</div>
                        <div class='template-row age'>${user_record.age_group}</div>
                        <div class='template-row gender'>${user_record.gender}</div>
                        <div class='template-row'>${user_record.address}</div>
                        <div class='template-row record-survey-row '>
                            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#a${user_record.user_id}">
                            問卷紀錄
                            </button>
                            <div id="a${user_record.user_id}" class="collapse record">
                                1.您想在治療中討論和解決哪些問題?<br>
                                    <span class="ans">${user_record.response.Symptom}</span><br>
                                2.您的治療目標是什麼?（請盡量分享你的期望）<br>
                                    <span class="ans">${user_record.response.intention}</span><br>
                                精神健康狀況，以便提供最適合的方案。<br>
                                3. 您會幾經常使用酒精或毒品?<br>
                                    <span class="ans">${user_record.response.Habits}</span><br>
                                4. 您目前正在服用任何處方藥/精神藥物嗎?<br>
                                    <span class="ans">${user_record.response.MentalState1}</span><br>
                                5. 您曾否得過任何心理輔導或精神上的支援服務嗎?<br>
                                    <span class="ans">${user_record.response.MentalState2}</span><br>
                                6. 您目前有任何自殘行為嗎?<br>
                                    <span class="ans">${user_record.response.MentalState3}</span><br>
                                7. 您現在有想要嚴重傷害任何人嗎?<br>
                                    <span class="ans">${user_record.response.MentalState4}</span><br>
                                8. 您曾經嘗試過自殺嗎?<br>
                                    <span class="ans">${user_record.response.MentalState5}</span><br>
                                9. 您現在有自殺的念頭嗎?<br>
                                    <span class="ans">${user_record.response.MentalState6}</span><br>
                                10. 您最後一次有計畫自殺是什麼時候?<br>
                                    <span class="ans">${user_record.response.MentalState7}</span><br>
                            </div>
                        </div>
                        <div class='template-row last'></div>
                    </div>
                </li >
            </ul>    
        `;
        // console.log(user_record.email);
    }
    var ans = document.getElementsByClassName("template-row");

    // console.log(ans[12].innerHTML);
    for (var i = 0; i < ans.length; i++) {
        if (ans[i].innerHTML == "null" || ans[i].innerHTML =="nullnull"){
            ans[i].innerHTML = " ";
        } else {
            ans[i].innerHTML = ans[i].innerHTML;
        };
            
    }
}
showPatientInfo();
// showRecord();

$(document).ready(function(){
    $("#myInput").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#myList li").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });

