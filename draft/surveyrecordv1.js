//18/7 draftv1 for getting survey record


async function showRecord(){ //Show record for Q7 to Q16
    const res = await fetch('/survey');
    const survey_records = await res.json();
    
    const surveyContainer = document.querySelector('.survey-container');
    surveyContainer.innerHTML = '';

    for(let survey_record of survey_records){
        // if(survey_record.patient_id)
        surveyContainer.innerHTML += `
                <div class="survey-intro">
                問卷記錄<br>
            7. 您想在治療中討論和解決哪些問題?(可多項選擇)這個問題必須回答 *<br>
                ${survey_record.response.Symptom}<br>
            8. 您的治療目標是什麼？（請盡量分享你的期望）。 <br>
                ${survey_record.response.intention}<br>
            精神健康狀況，以便提供最適合的方案。<br>
            9. 您會幾經常使用酒精或毒品？這個問題必須回答*<br>
                ${survey_record.response.Habits}<br>
            10. 您目前正在服用任何處方藥/精神藥物嗎？<br>
                ${survey_record.response.MentalState1}<br>
            11. 您曾否得過任何心理輔導或精神上的支援服務嗎？（例如和治療師，精神病醫生）?<br>
                ${survey_record.response.MentalState2}<br>
            12. 您目前有任何自殘行為嗎？<br>
                ${survey_record.response.MentalState3}<br>
            13. 您現在有想要嚴重傷害任何人嗎？<br>
                ${survey_record.response.MentalState4}<br>
            14. 您曾經嘗試過自殺嗎？<br>
                ${survey_record.response.MentalState5}<br>
            15. 您現在有自殺的念頭嗎？<br>
                ${survey_record.response.MentalState6}<br>
            16. 您最後一次有計畫自殺是什麼時候？<br>
                ${survey_record.response.MentalState7}<br>
          </div>
        `;
    }
}


async function showPatientInfo(){ //show record for Q1-Q6
    const res = await fetch('/users');
    const user_records = await res.json();
    const userContainer = document.querySelector('.user-container');
    userContainer.innerHTML = '';

    for(let user_record of user_records){
        // if(survey_record.patient_id)
        userContainer.innerHTML += `
                <div class="survey-intro">
                問卷記錄<br>
            1. 您的姓是什麼？這個問題必須回答 *<br>
                ${userContainer.firstName}<br>
            2. 您的名稱是什麼？這個問題必須回答 *<br>
                ${userContainer.lastName}<br>
            3. 請提供您的主要電子郵件地址？我們將使用此地址與您聯繫，以便將來進行通信。這個問題必須回答 *<br>
                ${userContainer.email}<br>
            4. 您是以下那一個年齡羣組？這個問題必須回答 *<br>
                ${userContainer.YourAge}<br>
            5. 您的性別是什麼？這個問題必須回答 *<br>
                ${userContainer.Gender}<br>
            6. 您目前居住的地方在那裡？這個問題必須回答*<br>
                ${userContainer.YourLocation}<br>
            </div>
        `;
    }
}

showPatientInfo();
showRecord();

// //socket IO?
// ///
// //reusable materials
// const socket = io.connect();

// socket.on('new-memo',function(memo){
//     const memosContainer = document.querySelector('.memos-container');
//     memosContainer.innerHTML += `
//                 <div class="memos">
//                     <div contenteditable='true' class="memo-content">
//                         ${memo.content}
//                     </div>
//                     <img src='/uploads/${memo.image}' 
//                         alt="${memo.content}"
//                         class="img-fluid" />
//                     <div class="edit-button" data-id='${memo.id}'>
//                         <i class="fas fa-edit" >
//                         </i>
//                     </div>
//                     <div class="trash-button" data-id='${memo.id}'>
//                         <i class="fas fa-trash"></i>
//                     </div>
//                 </div>
//         `;
// });

//reusable materials
{/* <div class="memos">
<div contenteditable='true' class="memo-content">
    ${memo.content}
</div>
<img src='/uploads/${memo.image}' 
    alt="${memo.content}"
    class="img-fluid" />
<div class="edit-button" data-id='${memo.id}'>
    <i class="fas fa-edit" >
    </i>
</div>
<div class="trash-button" data-id='${memo.id}'>
    <i class="fas fa-trash"></i>
</div>
</div> */}