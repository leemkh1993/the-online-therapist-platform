import * as bcryptjs from "bcryptjs";

const salt = 10;

export async function hashPW(pw: string) {
    const hash = await bcryptjs.hash(pw, salt);
    return hash;
};

// hashPW("the9@gmail.com").then(console.log);

export async function checkPW(pw: string, hashPw: string) {
    const match = await bcryptjs.compare(pw, hashPw);
    return match;
};

hashPW('123').then(hash => console.log(hash))