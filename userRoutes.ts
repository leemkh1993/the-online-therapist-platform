import express from 'express';
import {database} from './server'
import { checkPW } from './hash';

// Use this ts as middleware and export to other ts files
export const userRoutes = express.Router();

// Login POST method
userRoutes.post('/login', async function (req, res) {
    try {
        const email: string = req.body.loginEmail; // Get loginEmail input
        const pw: string = req.body.loginPassword; // Get loginPassword input
        const users = await database.query(/*sql*/ `Select * from users where users.email = $1`, [email]); // Get data from DB
        const user = users.rows[0]; // Draw user data from DB respond

        if (user != undefined) { // Check if user exists
            const match = await checkPW(pw, user.password); // Check pw, return boolean 
            if (match) {
                if (req.session) {
                    const role = await database.query(/*sql*/ `Select * from therapists join users on users.user_id = therapists.user_id 
                                                where users.email = $1`, [email]); // Get therapist data from DB, limit to input email only
                    if (role.rows[0] != undefined) {
                        req.session.email = email; // If true, create user in request cookie
                        req.session.role = "Therapist"; // If true, create role in request cookie
                        res.status(200).json({ success: true, role: "Therapist" });// 15/7 20:00, for testing by LC
                    } else {
                        req.session.email = email;
                        req.session.role = "Patient"; // Same logic as above
                        res.status(200).json({ success: true, role: "Patient" });// 15/7 20:00, for testing by LC
                    }
                }
            } else {
                res.status(401).json({WrongInfo: true}); // Return if PW wrong
            }
        } else {
            res.status(401).json({WrongInfo: true}); // Return if user wrong
        }
    } catch (e) {
        console.log(e);
        console.log("u encountered into catch");
    }
})

