//25.7 showname-test


let name = localStorage.getItem("username");
console.log(name);


console.log(localStorage.getItem("username"));

async function showName() {
	const res = await fetch('/username_display');
	const user_records = await res.json();

	const nameContainer = document.querySelector('.name-test');
	nameContainer.innerHTML = '';

	for (let user_record of user_records) {
		if (name == user_record.email) {
			nameContainer.innerHTML += `<div>${user_record.last_name}${user_record.first_name}</div>`;
			// 	nameContainer.innerHTML += `
			// 	<div>${name}</div>
			// `;
		} else {
		}
	}


	// console.log(ans[12].innerHTML);
	var name2 = $('.name-test').html();
	if (name2 == "null" || name2 == "nullnull") {
		$('.name-test').html("新用戶");
	} else {
		// nameContainer.innerHTML = nameContainer.innerHTML;
		name2 = name2;
	};

};
showName();

/* Jitsi */

$(document).ready(function () {
	$('[data-toggle="popover"]').popover({
		//  trigger: 'focus',
		trigger: 'hover',
		html: true,
		dataContainer: '.modal-body',
		//  placement: top,
		content: function () {
			return '<img class="img-fluid" src="' + $(this).data('img') + '" />';
		},
		title: 'Toolbox'
	})
});



function TalkToUsNow() {
	const popup = document.getElementById("popUpMsg");
	popup.classList.toggle("show");
}

/* Bootstrap scrollspy */
// jquery ready start
//  Activate bootstrap scrollspy
$(document).ready(function () {
	// jQuery code

	////////////////////////  Highlight the top nav as scrolling occurs. /bootstrap/
	$('body').scrollspy({
		target: '#navbar',
		offset: 80
	});
	//////////////////////// Menu scroll to section
	$('.scrollto').click(function (event) {
		var $anchor = $(this);
		$('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 40 }, 1500);
		event.preventDefault();
	});
	///////////////// for small screen
	if ($(window).width() < 992) {

		/////////////////////// Closes the Responsive Menu on Menu Item Click
		$('.navbar-collapse a.scrollto').click(function () {
			$('.navbar-toggler:visible').click();
		});
	}
});


/* toast box for notification */
$(document).ready(function () {
	$('.toast').toast('show');
});

/* Close the popup Box (x)
25 Jul: checked by Charlie: this is working */
function closeThePopUpBox() {
	const closebtns = document.getElementsByClassName("close");
	const allpopUpBoxElements = document.querySelector("#openbtn");

	for (let closebtn of closebtns) {
		closebtns[0].addEventListener("click", function () {
			allpopUpBoxElements.style.display = "none";
		});
	}
}

closeThePopUpBox();

/* Every (.close) Buttons in HTML : 
25 Jul: checked by Charlie: this is working */
function closeSpanX() {
	const closebtns = document.getElementsByClassName("close");
	for (i = 0; i < closebtns.length; i++) {
		closebtns[i].addEventListener("click", function () {
			this.parentElement.style.display = 'none';
		});
	};
}
closeSpanX();

/* Open Buttons functionable 
25 Jul: checked by Charlie: this is working */

function openThePopUpBox() {
	let openbtns = document.getElementsByClassName("open");
	let x;

	for (x = 0; x < openbtns.length; x++) {
		openbtns[x].addEventListener("click", function () {
			document.getElementById("openbtn").style.display = "";
			document.getElementById("openbtn2").style.display = "";
			console.log('clicked open btn');
		});
	}
}
openThePopUpBox();


/* Response to "現在諮詢" Button
25 Jul: checked by Charlie: this is working */
function TalkToUsNow() {
	const popup = document.getElementById("popUpMsg");
	popup.classList.toggle("show");
}

/* Pop up message selection 
25 Jul: checked by Charlie: this is working */

// SocketIO stuff

// Start a socket
const socket = io.connect();

// Update Online status stuff

// Server emit which therapist has just got online
socket.on('online', function (onlineTherapists) {
	// Data is therapist name (string)
	// DOM for changing the color to green and change word from available
	const allElements = Array.from(document.querySelectorAll(".online-therapist"));
	for (let therapist of onlineTherapists) {
		for (let element of allElements) {
			const id = element.getAttribute('id');
			if (id === therapist) {
				element.querySelector("div > div > svg").classList.remove("text-secondary");
				element.querySelector("div > div > svg").classList.add("text-success");
				element.querySelector("div > div > span").innerHTML = "&nbsp; ONLINE";
			} else {
				continue;
			};
		};
	};
});

// Server emit which therapist has just got offline
socket.on('offline', function (data) {
	// Data is therapist name (string)
	// DOM for changing the color to black and change word to offline
	const allElements = Array.from(document.querySelectorAll(".online-therapist"));
	for (let element of allElements) {
		const id = element.getAttribute('id');
		if (id === data) {
			element.querySelector("div > div > svg").classList.remove("text-success");
			element.querySelector("div > div > svg").classList.add("text-secondary");
			element.querySelector("div > div > span").innerHTML = "&nbsp; OFFLINE";
		} else {
			continue;
		};
	};
});

// Server notification for whom will be busy
socket.on('busy', function (busyTherapists) {
	// Data is therapist name (string)
	// DOM for changing the color to red and change word to busy
	const allElements = Array.from(document.querySelectorAll(".online-therapist"));
	for (let therapist of busyTherapists) {
		for (let element of allElements) {
			const id = element.getAttribute('id');
			if (id === therapist) {
				element.querySelector("div > div > svg").classList.remove("text-success");
				element.querySelector("div > div > svg").classList.add("text-warning");
				element.querySelector("div > div > span").innerHTML = "&nbsp; BUSY";
			} else {
				continue;
			};
		}
	};
});

// Update Online status stuff END

// Video Session Related Stuff
let request = true;

// Socket for receiving request from patients
// Data = { patientName: patientName, patientRoomKey: patientRoomKey, therapistRoomKey: therapistRoomKey, matchRoomKey: matchRoomKey }
socket.on('sessionRequest', function (data) {
	const navContainer = document.querySelector(".modal-dialog-2");
	navContainer.innerHTML = " ";
	navContainer.innerHTML += `
		<div class="modal-content-2">
			<div class="modal-header-2">
	  			<h4 class="modal-title-2" id="myModalLabel">
					你被邀請到${data.patientName}的視像房間！
	  			</h4>
			</div>
			<div class="modal-body-2">
	  			你確認現在跟${data.patientName}進行視像通話嗎？
	  			請按確認進入視頻，否則按取消離開。
			</div>
			<div class="modal-2-buttons">
	  			<button onclick="confirmSession('${data.patientRoomKey}', '${data.therapistRoomKey}', '${data.matchRoomKey}')" type="button" class="btn btn-default popup" data-dismiss="modal">
					進入Jitsi視像會議
	  			</button>
	  			<button onclick="closeModal2()" type="button" class="btn btn-default popup" data-dismiss="modal">
					取消
	  			</button>
			</div>
  		</div>
	`;
	navContainer.classList.remove('d-none')
});

// DOM for clicking on confirm button when confirm to start session
// Data = {patientRoomKey: value, therapistRoomKey: value, matchRoomKey: value}
function confirmSession(patientRoomKey, therapistRoomKey, matchRoomKey) {
	console.log(patientRoomKey, therapistRoomKey, matchRoomKey)
	const navContainer = document.querySelector(".modal-dialog-2");
	navContainer.classList.add('d-none')
	socket.emit('busy');
	socket.emit('startSession', patientRoomKey, matchRoomKey);
	// DOM manipulation, add jitsi textbox submit button etc.
	document.querySelector("#session-container").classList.remove("d-none");
	const jitsiContainer = document.querySelector("#submit-session");
	jitsiContainer.innerHTML = " ";
	jitsiContainer.innerHTML += `
		<textarea id="text-area" name="sessionNote"></textarea>
		<input class="d-none" name="matchKey" value="${matchRoomKey}">
		<button type="submit" class="btn session-submit-btn" id="sessionSubmit">Submit Session</button>
		<button onclick="window.location = '/therapist'" type="button" class="btn session-submit-btn">Back To Main Page</button>
	`;
	const domain = "meet.jit.si";
	const options = {
		roomName: matchRoomKey,
		width: "100%",
		height: "95%",
		parentNode: document.querySelector('#meet')
	}
	const api = new JitsiMeetExternalAPI(domain, options);
};

document.querySelector("#submit-session")
	.addEventListener('submit', async function (event) {
		event.preventDefault()

		const form = event.target;
		const formObject = {};

		formObject['note'] = form.sessionNote.value;
		formObject['matchKey'] = form.matchKey.value;

		const res = await fetch('/therapist/sessionSubmit', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(formObject),
		});

		const result = await res.json();

		if (res.status === 200 && result.recorded) {
			document.querySelector("#sessionSubmit").innerHTML = "Success!";
		}
	});

function closeModal2() {
	const container = document.querySelector('.modal-dialog-2');
	container.classList.add('d-none');
}

// DOM for clicking on submission button after session
// document.querySelector("")
// 	.addEventListener("click", function (event) {
// 		socket.emit('online');
// 		// DOM for submitting data to DB (need match ID, therapistID, patientID, text in textbox), closing the popup box
// 	});

// server-side work
// const uniqueNumber = xxx  // timestamp + random_number()
// input uniqueNumber into html


