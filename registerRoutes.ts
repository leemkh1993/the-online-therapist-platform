import express from "express";
export { database } from './server';
import { hashPW } from "./hash"; 
import bodyParser from "body-parser";
import { database } from "./server";

export const registerRoutes = express.Router();

console.log("registerRoutes connected");

registerRoutes.use(bodyParser.urlencoded({ extended: true })); // Form submit
registerRoutes.use(bodyParser.json()); // json content

// Come here when signing up
registerRoutes.post("/register", async function (req, res) {
    const loginEmail = req.body.loginEmail; // Get input email
    const checking = await database.query(/*sql*/ `select * from users where users.email = $1`, [loginEmail]); // Get data from DB
    if (checking.rows[0] == undefined) { // Check if email is used
        const loginPassword = req.body.loginPassword; // Get input password
        const hashPassword = await hashPW(loginPassword); // hash the input password, return a string

        // Create a row in users table in DB, pass in input email and hashed password
        const inputUser = await database.query(/*sql*/ `insert into users ( 
        email,
        password,
        created,
        updated
        ) values ($1, $2, NOW(), NOW())`, [loginEmail, hashPassword]);

        console.log(inputUser.rows[0])
        // Put the user into the patients table
        const inputRole = await database.query(/*sql*/ `insert into patients (user_id) values 
                                                ((select user_id from users where email = $1));`, [loginEmail]);
        const getuserId = await database.query(/*sql*/`select user_id from users where email = $1 LIMIT 1`, [loginEmail]);
        const userId = getuserId.rows[0].user_id;
        const condition: object = {
            "Symptom": "",
            "intention": "",
            "Habits": "",
            "MentalState1": "",
            "MentalState2": "",
            "MentalState3": "",
            "MentalState4": "",
            "MentalState5": "",
            "MentalState6": "",
            "MentalState7": ""
        }
        const inputRole2 = await database.query(/*sql*/ `insert into survey_records (patient_id, response) values 
                                                ((select patient_id from patients where user_id = $1), $2)`, [userId, condition]);
        const getpatId = await database.query(/*sql*/`select patient_id from patients where user_id = $1 LIMIT 1`, [userId]);
        const patId = getpatId.rows[0].patient_id;
        const getsurId = await database.query(/*sql*/ `select survey_record_id from survey_records where patient_id = $1 LIMIT 1`, [patId]);
        const surId = getsurId.rows[0].survey_record_id;
        console.log(`user ID${patId} is registered with survey ID${surId}`);
        const inputRole3 = await database.query(/*sql*/ `
                                                update patients 
                                                set survey_record_id = ($1)
                                                where patient_id = ($2)
                                                `,[surId,patId]);

        console.log(inputRole.rows[0]);
        console.log(inputRole2.rows[0]);
        console.log(inputRole3.rows[0]);

        // Assign values into cookies so user can login straight after signing up
        if (req.session) {
            req.session.email = loginEmail;
            req.session.role = "Patient";
        }
        res.status(200).json({success: true, role: "Patient"});
    } else {
        res.status(401).json({EmailTaken: true});
    }
});