import express from 'express';
import {database} from './server';

export const therapistRoutes = express.Router();

type patientData = {
    firstName: string,
    lastName: string,
    session: JSON[]
}

therapistRoutes.get('/therapist/patient_record', async function (req, res) {
    try {
        if (req.session) {
            // Get current therapist
            const email = req.session.email;
            // Get therapist id
            const getId = await database.query(/*sql*/ `select * from therapists join users on therapists.user_id = users.user_id where users.email = $1`, [email]);
            const therapistId = getId.rows[0].therapist_id;
            const getMatches = await database.query(/*sql*/ `select * from matches  where matches.therapist_id = $1`, [therapistId]);
            const matches = getMatches.rows;

            if (matches != undefined) { // Check if any match found
                const patientsData: patientData[] = [] // For JSON of each patient

                for (let match of matches) {
                    const matchId = match.match_id; 
                    const patientId = match.patient_id;
                    const getNames = await database.query(/*sql*/ `select * from patients join users on patients.user_id = users.user_id where patients.patient_id = $1`, [patientId]);
                    const firstName = getNames.rows[0].first_name;
                    const lastName = getNames.rows[0].last_name;

                    const getSessions = await database.query(/*sql*/ `select * from thera_sessions where thera_sessions.match_id = $1 order by date`, [matchId])
                    const sessions: JSON[] = []

                    for (let session of getSessions.rows) {
                        sessions.push(session)
                    }

                    const patientData = {
                        firstName: firstName,
                        lastName: lastName,
                        sessions: sessions
                    }
                    // patientsData.push(patientData)
                    console.log(patientData)
                }
                res.status(200).json(patientsData);
            } else {
                res.status(204).json({emptyList: true});
            }
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({AccessFailed: true});
    }
});

therapistRoutes.post('/therapist/sessionSubmit', async function (req, res) {
    try {
        if (req.session) {
            const matchKey = req.body.matchKey;
            console.log(req.body)
            const getMatchId = await database.query(/*sql*/ `select match_id from matches where matches.room_key = $1`, [matchKey])
            const note = req.body.sessionNote;
            const createSession = await database.query(/*sql*/ `insert into thera_sessions (date, match_id, note) values (now(), $1, $2)`, [getMatchId.rows[0].match_id, note]);
            console.log(createSession.rows);
            res.status(200).json({recorded: true})
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({SubmitFailed: true})
    }
});